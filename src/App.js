import React from 'react';
import { Footer, Header } from './components';
import { AppRoutes } from './routes/AppRoutes';

function App() {
  return (
    <div className='App dark:bg-slate-800'>
      <Header/>
      <AppRoutes/>
      <Footer/>
    </div>
  )
}

export default App;

