import React, { useEffect, useRef, useState } from "react";
import Logo from "../../../assets/light-logo.png";
import DarkLogo from "../../../assets/dark-logo.png";
import { Link } from "react-router-dom";
import { Search } from "./Search";

import { DropdownLoggedOut } from "./DropdownLoggedOut";
import { DropDownLoggedIn } from "./DropDownLoggedIn";


export const Header = () => {
  const [darkMode, setDarkMode] = useState(
    JSON.parse(localStorage.getItem("darkMode")) || false
  );
  const [dropdown, setDropdown] = useState(false);
  const dropdownRef = useRef(null);
  const [searchSection, setSearchSection] = useState(false);

  useEffect(() => {
    localStorage.setItem("darkMode", JSON.stringify(darkMode));

    if (darkMode) {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [darkMode]);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        // Click occurred outside the dropdown. Hence, close it
        setDropdown(false);
      }
    };

    // Attach event listener
    document.addEventListener("mousedown", handleClickOutside);

    // Clean up the event listener on component unmount
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [dropdownRef]);

  const token = JSON.parse(sessionStorage.getItem("token") || false);
  console.log(token ? 'set' : 'unset')

  return (
    <header>
      <nav className="w-full bg-primary-700 dark:bg-slate-900 dark:border-b border-gray-600 dark:bg-gray-900">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
          <Link
            to="/"
            className="flex items-center space-x-3 rtl:space-x-reverse"
          >
            <img
              src={darkMode ? DarkLogo : Logo}
              className="h-14"
              alt="Purity Plants Logo"
            />
            <span className="sr-only">Purity Plants</span>
          </Link>
          <div className="flex items-center relative">
            <ul className="flex flex-col font-medium p-4 md:p-0 mt-4 border rounded-lg  md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 dark:bg-slate-900 bg-primary-700">
              <li>
                <Link
                  to="/"
                  className="block py-2 px-3 rounded text-white dark:bg-slate-900 hover:text-slate-800 md:p-0 dark:text-primary-800 dark:hover:text-white"
                  aria-current="page"
                >
                  Home
                </Link>
              </li>
              <li>
                <Link
                  to="/products"
                  className="block py-2 px-3 rounded text-white dark:bg-slate-900 hover:text-slate-800 md:p-0 dark:text-primary-800 dark:hover:text-white"
                >
                  Products
                </Link>
              </li>
            </ul>
          </div>
          <div className="flex items-center relative">
            <span
              className={`hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-${
                darkMode ? "sun-fill" : "moon-fill"
              }`}
              onClick={() => setDarkMode(!darkMode)}
            ></span>
            <span
              onClick={() => setSearchSection(!searchSection)}
              className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-search"
            ></span>
            <Link to="/cart">
              <span className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-cart"></span>
            </Link>
            <span
              onClick={() => setDropdown(!dropdown)}
              className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-person-circle"
            ></span>
            {
              dropdown && 
              (token ? <DropDownLoggedIn setDropdown={setDropdown} refProp={dropdownRef}/> : <DropdownLoggedOut setDropdown={setDropdown} refProp={dropdownRef}/>)
            }
          </div>
        </div>
      </nav>
      {searchSection && <Search setSearchSection={setSearchSection} />}
    </header>
  );
};
