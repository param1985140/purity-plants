import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { CartPage, DashboardPage, HomePage, Login, ProductDetailPage, ProductPage, Register } from '../pages'


export const AppRoutes = () => {
  return (
    <>
        <Routes>
            <Route path="/" element={<HomePage title = "Home | Purity Plants"/>}/>
            <Route path='/products' element={<ProductPage title = "Products | Purity Plants"/>}/>
            <Route path='/products/:id' element={<ProductDetailPage/>}/>
            <Route path='/register' element={<Register/>}/>
            <Route path='/login' element={<Login/>}/>
            <Route path='/dashboard' element={<DashboardPage/>}/>
            <Route path='/cart' element={<CartPage/>}/>
        </Routes>
    </>
  )
}
