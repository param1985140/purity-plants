import React, { useEffect } from 'react'
import { Rating } from '../../components/Elements/Rating'
import { useParams } from 'react-router-dom';
import { DetailProduct } from './components/DetailProduct';
import { useFetch } from '../../hooks';

 

export const ProductDetailPage = () => {
  const params = useParams();
  const {data: product , setUrl , isLoading} = useFetch();

  useEffect(() => {
    const productId = params.id;
    const URL = `http://localhost:8000/products/${productId}`;
    setUrl(URL);
  },[params.id, setUrl]);

  return (
      <main>
        {product && <DetailProduct key={product.id} product={product}/>}
      </main>
    )
} 

