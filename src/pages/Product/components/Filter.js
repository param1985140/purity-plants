import React from "react";
import { useFilter } from "../../../context/FilterContext";

export const Filter = ({ show, setShow }) => {
  const { state, dispatch } = useFilter();
  const allRequiredClass =
    " fixed top-0 left-0 z-40 h-screen p-4 overflow-y-auto transition-transform bg-white w-64 dark:bg-gray-800";

  const offCanvasClass = show
    ? `${allRequiredClass} transform-none`
    : `${allRequiredClass} -translate-x-full`;

  return (
    <div
      id="drawer-disable-body-scrolling"
      className={offCanvasClass}
      tabindex="-1"
      aria-labelledby="drawer-disable-body-scrolling-label"
    >
      <h5
        id="drawer-disable-body-scrolling-label"
        className="text-base font-semibold text-gray-500 uppercase dark:text-gray-400"
      >
        Filters
      </h5>
      <button
        type="button"
        data-drawer-hide="drawer-disable-body-scrolling"
        aria-controls="drawer-disable-body-scrolling"
        className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 absolute top-2.5 end-2.5 inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white"
        onClick={() => setShow(false)}
      >
        <svg
          className="w-3 h-3"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 14 14"
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
          />
        </svg>
        <span className="sr-only">Close menu</span>
      </button>
      <div className="py-4 overflow-y-auto">
        <div>
          <h2 className="font-bold dark:text-slate-100">Sort By</h2>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="lowToHigh">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="lowToHigh"
                onChange={() => dispatch({ type: "SORT_BY" , payload: { sortBy: "lowtohigh"} })} 
                checked={state.sortBy === "lowtohigh"}
              />
              <span className="ml-2"></span>
              Price - Low to High
            </label>
          </div>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="highToLow">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="highToLow" 
                onChange={() => dispatch({ type: "SORT_BY" , payload: { sortBy: "hightolow"} })}
                checked={state.sortBy === "hightolow"} 
              />
              <span className="ml-2"></span>
              Price - High to Low
            </label>
          </div>
        </div>
        <div className="mt-5">
          <h2 className="font-bold dark:text-slate-100">Rating</h2>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="fourNAbove">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="fourNAbove" 
                name="rating"
                onChange={() => dispatch({type: "RATINGS" , payload: { ratings: "4STARSANDABOVE"}})}
                checked={state.ratings === "4STARSANDABOVE"}
              />
              <span className="ml-2"></span>4 Stars &amp; Above
            </label>
          </div>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="threeNAbove">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="threeNAbove"
                name="rating" 
                onChange={() => dispatch({type: "RATINGS" , payload: { ratings: "3STARSANDABOVE"}})}
                checked={state.ratings === "3STARSANDABOVE"}
              />
              <span className="ml-2"></span>3 Stars &amp; Above
            </label>
          </div>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="twoNAbove">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="twoNAbove" 
                name="rating" 
                onChange={() => dispatch({type: "RATINGS" , payload: { ratings: "2STARSANDABOVE"}})}
                checked={state.ratings === "2STARSANDABOVE"}
              />
              <span className="ml-2"></span>2 Stars &amp; Above
            </label>
          </div>
          <div className="form-control dark:text-slate-100">
            <label htmlFor="oneNAbove">
              <span className="ml-2"></span>
              <input 
                type="radio" 
                id="oneNAbove" 
                name="rating"
                onChange={() => dispatch({type: "RATINGS" , payload: { ratings: "1STARSANDABOVE"}})}
                checked={state.ratings === "1STARSANDABOVE"}
              />
              <span className="ml-2"></span>1 Star &amp; Above
            </label>
          </div>
        </div>
      </div>
      <div className="mt-5">
        <h2 className="font-bold dark:text-slate-100">Other Filters</h2>
        <div className="form-control dark:text-slate-100">
          <label htmlFor="bestSeller">
            <span className="ml-2"></span>
            <input
              className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
              type="checkbox"
              id="bestSeller"
              onChange={() => dispatch({type: "BEST_SELLER_ONLY" , payload: { bestSellerOnly: !state.bestSellerOnly} })}
              checked={state.bestSellerOnly}
            />
            <span className="ml-2"></span>
            Best Seller
          </label>
        </div>
        <div className="form-control dark:text-slate-100">
          <label htmlFor="inStockOnly">
            <span className="ml-2"></span>
            <input
              className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
              type="checkbox"
              id="inStockOnly"
              onChange={() => dispatch({type: "ONLY_IN_STOCK" , payload: { onlyInStock: !state.onlyInStock} })}
              checked={state.onlyInStock}
            />
            <span className="ml-2"></span>
            In Stock Only
          </label>
        </div>
        <div className="mt-5 text-center">
          <button 
            className="text-white bg-primary-700 hover:bg-primary-900 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-900 focus:outline-none dark:focus:ring-blue-800"
            onClick={() => dispatch({type: "CLEAR_FILTER"})}
          >
            Clear Filter
          </button>
        </div>
      </div>
    </div>
  );
};
