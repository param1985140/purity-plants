import React from "react";
import { Rating } from "../../../components/Elements/Rating";

export const DetailProduct = ({ product }) => {
  return (
    <div>
      <h1 className="mt-10 mb-5 text-4xl text-center font-bold text-slate-900 dark:text-slate-200">
        {product.name}
      </h1>
      <p className="mb-5 text-lg text-center text-slate-900 dark:text-slate-200">
        {product.overview}
      </p>
      <div className="flex flex-wrap justify-around">
        <div className="max-w-lg my-3">
          <img src={product.poster} alt={product.name} />
        </div>
        <div className="max-w-xl my-3 ml-3">
          <p className="text-3xl font-bold text-gray-900 dark:text-slate-200">
            <span className="mr-1">$</span>
            <span>{product.price}</span>
          </p>
          <p>
            <div className="flex items-center">
              <p className="text-primary-900 text-xs font-semibold py-0.5 rounded dark:bg-slate-800 dark:text-primary-800">
                <Rating rating={product.rating} />
              </p>
            </div>
          </p>
          <p className="my-4 select-none">
            {product.best_seller 
            ? <span className="font-semibold text-yellow-600	border bg-yellow-100 rounded-lg px-3 py-1 mr-2">BEST SELLER</span> 
            : 
            " "
            }
            { !product.in_stock 
              ? 
              <span className="font-semibold text-rose-600	border bg-slate-100 rounded-lg px-3 py-1 mr-2">OUT OF STOCK</span> 
              : 
              <span className="font-semibold text-emerald-600	border bg-slate-100 rounded-lg px-3 py-1 mr-2">IN STOCK</span>
            }
            <span className="font-semibold text-blue-500 border bg-slate-100 rounded-lg px-3 py-1 mr-2">
              {product.size} Inches
            </span>
          </p>
          <p className="my-3">
            <button className={`text-white cursor:pointer bg-primary-700 hover:bg-primary-800 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none 
            ${
              product.in_stock ? "" : "cursor-not-allowed"
            }`}
            disabled={product.in_stock ? "" : "disabled"}
            >
              Add To Cart <i className="ml-1 bi bi-plus-lg"></i>
            </button>
          </p>
          <p className="text-md text-gray-900 dark:text-slate-200">
            {product.long_description}
          </p>
        </div>
      </div>
    </div>
  );
};
