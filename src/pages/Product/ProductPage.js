import React, { useEffect, useState } from "react";
import { Filter } from "./components/Filter";
import { ProductCard } from "../../components";
import { useSearchParams } from "react-router-dom";
import { useFilter } from "../../context/FilterContext";
import { useDynamicTitle, useFetch } from "../../hooks";
import { ProductCardSkeleton } from "../../components/Misc/ProductCardSkeleton";


export const ProductPage = ({ title }) => {

  const { products , setInitialProductList } = useFilter();
  console.log(products);
  const [show, setShow] = useState(false);
  const [searchParams] = useSearchParams();

  useDynamicTitle(title);

  function onProductFetch(data) {
    setInitialProductList(data);
  }

  const queryTerm = searchParams.get("q");
  const URL = `http://localhost:8000/products${
    queryTerm ? "?name_like=" + queryTerm : ""
  }`;
  const { setUrl,isLoading } = useFetch(URL,onProductFetch);

  useEffect(() => {
    setUrl(
      URL
    );
  }, [URL, queryTerm, setUrl]);

  function renderSkeletons(count) {
    const skeletons = [];
    for (let i=1;i<=count;i++) {
      skeletons.push(<ProductCardSkeleton key={i}/>);
    }
    return skeletons;
  }

  return (
    <main className="my-5">
      <div className="flex justify-between">
        <h2 className="text-2xl font-semibold dark:text-slate-100 mb-5 section-title">
          { products && products.length === 0 && "No Products Found"}
          { queryTerm && products && products.length !== 0 && `Products For: ${queryTerm} (${products.length})`}
          { !queryTerm && products && products.length !== 0 && `All Products (${products.length})`}
        </h2>
        <span>
          <button
            className="text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus:ring-blue-800"
            type="button"
            data-drawer-target="drawer-disable-body-scrolling"
            data-drawer-show="drawer-disable-body-scrolling"
            data-drawer-body-scrolling="false"
            aria-controls="drawer-disable-body-scrolling"
            onClick={() => setShow(!show)}
          >
            Filter
          </button>
        </span>
      </div>
      <Filter show={show} setShow={setShow} />
      <div className="flex flex-wrap justify-center lg:flex-row">
        {
          isLoading && renderSkeletons(20)
        }
        {
          products &&
          products.map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
      </div>
    </main>
  );
};
