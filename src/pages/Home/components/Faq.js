import React from 'react'
import { Accordion } from './Accordion'

export const Faq = () => {
    const faqs = [
        {
            id: 1,
            question: "Are Plants Good For Health ?",
            answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequuntur natus saepe? Reprehenderit, adipisci repudiandae iste quod dicta labore eligendi."
        },
        {
            id: 2,
            question: "Are Plants Safe ?",
            answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequuntur natus saepe? Reprehenderit, adipisci repudiandae iste quod dicta labore eligendi."
        },
        {
            id: 3,
            question: "Can Plants Grow in Artificial Light ?",
            answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequuntur natus saepe? Reprehenderit, adipisci repudiandae iste quod dicta labore eligendi."
        },
        {
            id: 4,
            question: "What Type Of Delivery Options Are Available For Plants ?",
            answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequuntur natus saepe? Reprehenderit, adipisci repudiandae iste quod dicta labore eligendi."
        }
    ]
  return (
    <div>
        <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
           FAQs
        </h2>
        <div id="accordion-open" data-accordion="open">
            {faqs.map(faq =>  <Accordion key={faq.id} question={faq.question} answer={faq.answer}/>)}
        </div>
    </div>
  )
}
