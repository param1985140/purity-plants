import React from 'react'
import { ProductCard } from '../../../components'
import { useFetch } from '../../../hooks'
import { ProductCardSkeleton } from '../../../components/Misc/ProductCardSkeleton'


export const FeaturedProducts = () => {
  const {data:featuredProducts, error, isLoading} = useFetch("http://localhost:8000/featured-products")

  function renderSkeletons(count) {
    const skeletons = [];
    for (let i=1;i<=count;i++) {
      skeletons.push(<ProductCardSkeleton key={i}/>);
    }
    return skeletons;
  }


  return (
    <section className="my-20">
        <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
            Featured Products
        </h2>
        <div className='flex flex-wrap justify-center lg:flex-row'>
            {
              isLoading && renderSkeletons(3)
            }
            {
              featuredProducts && featuredProducts.map(product => <ProductCard key={product.id} product={product}/>)
            }
        </div>
    </section>
  )
}
