import React from 'react'
import Slider from 'react-slick';
import { SliderCard } from './SliderCard';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const testimonial = [
    {
        id: 1,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod quaerat id optio sequi laboriosam ipsa sunt dolorem ducimus quibusdam explicabo.",
        author: "John Doe",
        company: "ABC Inc."
    },
    {
        id: 2,
        text: "Consequuntur velit nostrum assumenda quibusdam eos laudantium enim laboriosam porro excepturi quos Lorem ipsum dolor, sit amet consectetur adipisicing elit. Assumenda, debitis.",
        author: "William son",
        company: "PQR Inc."
    },
    {
        id: 3,
        text: "Laborum doloribus quia voluptatibus, ratione inventore qui fuga quo. Maiores, natus et Lorem ipsum dolor sit, amet consectetur adipisicing elit. Debitis, repellat.",
        author: "JJ Smith",
        company: "XYZ Inc."
    }
]

export default function Testimonial() {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };
  return (
    <div className='mb-12'>
        <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
            Pure reviews
        </h2>
        <Slider {...settings}>
           {
            testimonial.map(testimonial => 
                <SliderCard testimonial={testimonial}/>
            )
           }
        </Slider>
    </div>
  );
}
